package com.epam.view.menuServices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputViewInterface {
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


    public static int inputInteger(String line){
        while(true){
            System.out.print(line + ": ");
            try {
                return Integer.parseInt(br.readLine());
            } catch (IOException | NumberFormatException e) {
                e.printStackTrace();
                System.out.println("Invalid input. Please re-enter");
            }
        }
    }
}
