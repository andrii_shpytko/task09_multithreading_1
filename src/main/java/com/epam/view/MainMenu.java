package com.epam.view;

import com.epam.controller.Controller;
import com.epam.view.menuServices.InputViewInterface;
import com.epam.view.menuServices.Menu;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu implements Menu {
    private Controller controller;

    public MainMenu() {
        controller = new Controller();
    }

    @Override
    public String initNameOfMenu() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<String, String>() {
            {
                put("1", "1\tPing-pong game");
                put("2", "2\tFibonacci sequence");
                put("3", "3\tThread scheduled sleeping");
                put("5", "5\tPipe communication");
                put("Q", "Q\tExit");
            }
        };
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<String, Runnable>() {
            {
                put("1", () -> controller.runPingPongGame());
                put("2", () -> controller.printFibonacciSequence(InputViewInterface.inputInteger("length")));
                put("3", () -> controller.runThreadScheduledSleeping(InputViewInterface.inputInteger("quantity")));
                put("5", () -> controller.runPipeCommunication());
                put("Q", () -> System.out.println("\nBye"));
            }
        };
    }
}
