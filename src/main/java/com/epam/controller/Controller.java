package com.epam.controller;

import com.epam.model.fibonacci.FibonacciSequence;
import com.epam.model.game.PingPong;
import com.epam.model.pipeCommunication.PipeCommunication;
import com.epam.model.schedule.ThreadScheduledSleeping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.Arrays;

public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    public void runPingPongGame() {
        try {
            int duration = 5_000;
            System.out.println("\t" + LocalDateTime.now());
            new PingPong(System.out).runGame(duration);
            System.out.println("\t" + LocalDateTime.now());
            LOGGER.info("Ping-pong game has been executed; duration = " + duration + "ms");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            LOGGER.info("unable to execute ping-pong game; " + e.getMessage());
        }
    }

    public void printFibonacciSequence(int quantity) {
        try {
            FibonacciSequence fibonacciSequence = new FibonacciSequence(quantity);
            LOGGER.info("returned fibonacci sequence " + Arrays.toString(fibonacciSequence.getSequence()));
            LOGGER.info("sum of elements = " + fibonacciSequence.getSumOfSequenceElements());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            LOGGER.info("unable to get fibonacci sequence; " + e.getMessage());
        }
    }

    public void runThreadScheduledSleeping(int quantity) {
        try {
            new ThreadScheduledSleeping(quantity).start();
            LOGGER.info("thread scheduled sleeping task has been executed; quantity of threads = " + quantity);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("unable to execute thread scheduled sleeping task; " + e.getMessage());
        }
    }

    public void runPipeCommunication(){
        PipeCommunication.start();
        LOGGER.info("pipe communication has been executed");
    }
}
