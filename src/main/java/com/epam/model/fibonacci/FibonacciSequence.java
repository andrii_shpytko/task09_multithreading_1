package com.epam.model.fibonacci;

import java.util.Arrays;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FibonacciSequence {
    private int numb1;
    private int numb2;
    private int numb3;
    private AtomicInteger index;
    private int[] sequence;

    public FibonacciSequence(int quantity) {
        numb1 = 0;
        numb2 = 1;
        numb3 = numb1 + numb2;
        index = new AtomicInteger(3);
        buildSequence(quantity);
    }

    public int[] getSequence() {
        return Arrays.copyOf(sequence, sequence.length);
    }

    private void buildSequence(int length) {
        if (!isLengthValid(length)) {
            throw new IllegalArgumentException("invalid length value");
        }
        if (length == 1) {
            sequence = new int[]{numb1};
        } else if (length == 2) {
            sequence = new int[]{numb1, numb2};
        } else {
            buildSequenceForMoreThanTwoElements(length);
        }
    }

    private void buildSequenceForMoreThanTwoElements(int quantity) {
        sequence = new int[quantity];
        initializeFirstThreeElements();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Runnable updater = () -> updateFibonacciNumbers();
        Runnable writer = () -> sequence[index.getAndIncrement()] = numb3;
        while (index.get() < quantity) {
            try {
                executorService.submit(updater).get();
                executorService.submit(writer).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateFibonacciNumbers() {
        numb1 = numb2;
        numb2 = numb3;
        numb3 = numb1 + numb2;
    }

    private void initializeFirstThreeElements() {
        sequence[0] = numb1;
        sequence[1] = numb2;
        sequence[2] = numb3;
    }

    private boolean isLengthValid(int length) {
        return length > 0;
    }

    public int getSumOfSequenceElements(){
        Callable<Integer> sumOfElements = ()-> Arrays.stream(sequence).sum();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Integer> result = executorService.submit(sumOfElements);
        int sum;
        try {
            sum = result.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            sum = 0;
        }
        return sum;
    }
}
