package com.epam.model.game;

import java.io.PrintStream;

public class PingPong {
    private PrintStream printStream;
    private Runnable ping;
    private Runnable pong;

    public PingPong(PrintStream printStream) {
        this.printStream = printStream;
        ping = () -> makePing();
        pong = () -> makePong();
    }

    public void runGame(int duration) {
        if (!isDurationValid(duration)) {
            throw new IllegalArgumentException("invalid duration value");
        }
        Thread pingThread = new Thread(ping);
        Thread pongThread = new Thread(pong);
        startPingPongThreads(pingThread, pongThread);
        sleepMainGameThread(duration);
        interruptPingPongThreads(pingThread, pongThread);
        waitPingPongThreadsDie(pingThread, pongThread);
    }

    private void waitPingPongThreadsDie(Thread pingThread, Thread pongThread) {
        try {
            pingThread.join();
            pongThread.join();
        } catch (InterruptedException e) {
//            e.printStackTrace();
            System.err.println(Thread.currentThread().getName() + "was interrupted");
        }
    }

    private void interruptPingPongThreads(Thread pingThread, Thread pongThread) {
        pingThread.interrupt();
        pongThread.interrupt();
    }

    private void sleepMainGameThread(int duration) {
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
//            e.printStackTrace();
            System.err.println(Thread.currentThread().getName() + "was interrupted");
        }
    }

    private void startPingPongThreads(Thread pingThread, Thread pongThread) {
        pingThread.start();
        pongThread.start();
    }

    private boolean isDurationValid(int duration) {
        return duration >= 0;
    }

    private synchronized void makePing() {
        printStream.println("<<  -start ping-  >>");
        try {
            while (!Thread.currentThread().isInterrupted()) {
                wait();
                Thread.sleep(500);
                printStream.println("ping");
                notify();
            }
        } catch (InterruptedException e) {
//            e.printStackTrace();
        }
        printStream.println("<<  -finish ping-  >>");
    }

    private synchronized void makePong() {
        printStream.println("<<  -start pong-  >>");
        try {
            while (!Thread.currentThread().isInterrupted()) {
                notify();
                wait();
                Thread.sleep(500);
                printStream.println("pong");
                notify();
            }
        } catch (InterruptedException e) {
//            e.printStackTrace();
        }
        printStream.println("<<  -finish pong-  >>");
    }
}
