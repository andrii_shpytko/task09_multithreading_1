package com.epam.model.pipeCommunication;

import java.io.IOException;
import java.io.PipedReader;

public class Receiver extends Thread {
    private PipedReader input;

    Receiver(Sender sender) throws IOException {
        input = new PipedReader(sender.getPipedWriter());
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("Read: " + (char) input.read());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
