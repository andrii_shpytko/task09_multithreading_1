package com.epam.model.pipeCommunication;

import java.io.IOException;
import java.io.PipedWriter;

public class Sender extends Thread {
    private PipedWriter out;

    public Sender() {
        out = new PipedWriter();
    }

    public PipedWriter getPipedWriter() {
        return out;
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                for (char c = 'A'; c < 'z'; c++) {
                    out.write(c);
                    sleep(1_000);
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}

