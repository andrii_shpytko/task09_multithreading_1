package com.epam.model.pipeCommunication;

import java.io.IOException;

public class PipeCommunication {
    public static void start() {
        try {
            Sender sender = new Sender();
            Receiver receiver = new Receiver(sender);
            sender.start();
            receiver.start();
            try {
                Thread.sleep(10_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sender.interrupt();
            receiver.interrupt();
            try {
                sender.join();
                receiver.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
