package com.epam.model.schedule;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreadScheduledSleeping {
    private static final Logger LOGGER = LogManager.getLogger(ThreadScheduledSleeping.class);
    private ScheduledExecutorService scheduledExecutorService;

    public ThreadScheduledSleeping(int threadsQuantity) {
        if (threadsQuantity <= 0) {
            throw new IllegalArgumentException();
        }
        scheduledExecutorService = Executors.newScheduledThreadPool(threadsQuantity);
    }

    public void start() {
        Runnable taskOfSchedule = () -> {
            System.out.println(Thread.currentThread().getName() + "\tstarted execution");
            System.out.println(Thread.currentThread().getName() + "\tis sleeping...");
            System.out.println(Thread.currentThread().getName() + "\tfinished execution");
//            LOGGER.info(Thread.currentThread().getName() + "\tstarted execution");
//            LOGGER.info(Thread.currentThread().getName() + "\tis sleeping...");
//            LOGGER.info(Thread.currentThread().getName() + "\tfinished execution");
        };
        Random random = new Random();
        scheduledExecutorService.schedule(taskOfSchedule, random.nextInt(5), TimeUnit.SECONDS);
        scheduledExecutorService.schedule(taskOfSchedule, random.nextInt(5), TimeUnit.SECONDS);
        scheduledExecutorService.schedule(taskOfSchedule, random.nextInt(5), TimeUnit.SECONDS);
        scheduledExecutorService.schedule(taskOfSchedule, random.nextInt(5), TimeUnit.SECONDS);
        scheduledExecutorService.schedule(taskOfSchedule, random.nextInt(5), TimeUnit.SECONDS);
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        scheduledExecutorService.shutdown();
    }
}
